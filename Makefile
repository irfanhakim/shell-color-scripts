clean: 
	rm -rf /var/opt/shell-color-scripts

install: clean
	mkdir -p /var/opt/shell-color-scripts/colorscripts
	cp -rf colorscripts/* /var/opt/shell-color-scripts/colorscripts
	cp colorscript.sh /usr/local/bin/colorscript

uninstall:
	rm -rf /var/opt/shell-color-scripts
	rm -f /usr/local/bin/colorscript

